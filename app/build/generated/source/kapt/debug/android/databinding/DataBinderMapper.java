
package android.databinding;
import th.co.artxzilla.movierating.BR;
class DataBinderMapper  {
    final static int TARGET_MIN_SDK = 23;
    public DataBinderMapper() {
    }
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case th.co.artxzilla.movierating.R.layout.item_search_movie:
                    return th.co.artxzilla.movierating.databinding.ItemSearchMovieBinding.bind(view, bindingComponent);
                case th.co.artxzilla.movierating.R.layout.activity_search_movie:
                    return th.co.artxzilla.movierating.databinding.ActivitySearchMovieBinding.bind(view, bindingComponent);
                case th.co.artxzilla.movierating.R.layout.activity_movie_detail:
                    return th.co.artxzilla.movierating.databinding.ActivityMovieDetailBinding.bind(view, bindingComponent);
        }
        return null;
    }
    android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case -733590639: {
                if(tag.equals("layout/item_search_movie_0")) {
                    return th.co.artxzilla.movierating.R.layout.item_search_movie;
                }
                break;
            }
            case -1143531883: {
                if(tag.equals("layout/activity_search_movie_0")) {
                    return th.co.artxzilla.movierating.R.layout.activity_search_movie;
                }
                break;
            }
            case 1091194684: {
                if(tag.equals("layout/activity_movie_detail_0")) {
                    return th.co.artxzilla.movierating.R.layout.activity_movie_detail;
                }
                break;
            }
        }
        return 0;
    }
    String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"
            ,"data"
            ,"id"
            ,"imdbID"
            ,"poster"
            ,"title"
            ,"type"
            ,"year"};
    }
}