package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class MovieRealmProxy extends th.co.artxzilla.movierating.data.entity.Movie
    implements RealmObjectProxy, MovieRealmProxyInterface {

    static final class MovieColumnInfo extends ColumnInfo {
        long idIndex;
        long titleIndex;
        long yearIndex;
        long imdbIDIndex;
        long typeIndex;
        long posterIndex;

        MovieColumnInfo(SharedRealm realm, Table table) {
            super(6);
            this.idIndex = addColumnDetails(table, "id", RealmFieldType.INTEGER);
            this.titleIndex = addColumnDetails(table, "title", RealmFieldType.STRING);
            this.yearIndex = addColumnDetails(table, "year", RealmFieldType.STRING);
            this.imdbIDIndex = addColumnDetails(table, "imdbID", RealmFieldType.STRING);
            this.typeIndex = addColumnDetails(table, "type", RealmFieldType.STRING);
            this.posterIndex = addColumnDetails(table, "poster", RealmFieldType.STRING);
        }

        MovieColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new MovieColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final MovieColumnInfo src = (MovieColumnInfo) rawSrc;
            final MovieColumnInfo dst = (MovieColumnInfo) rawDst;
            dst.idIndex = src.idIndex;
            dst.titleIndex = src.titleIndex;
            dst.yearIndex = src.yearIndex;
            dst.imdbIDIndex = src.imdbIDIndex;
            dst.typeIndex = src.typeIndex;
            dst.posterIndex = src.posterIndex;
        }
    }

    private MovieColumnInfo columnInfo;
    private ProxyState<th.co.artxzilla.movierating.data.entity.Movie> proxyState;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("id");
        fieldNames.add("title");
        fieldNames.add("year");
        fieldNames.add("imdbID");
        fieldNames.add("type");
        fieldNames.add("poster");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    MovieRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (MovieColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<th.co.artxzilla.movierating.data.entity.Movie>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public Long realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.idIndex)) {
            return null;
        }
        return (long) proxyState.getRow$realm().getLong(columnInfo.idIndex);
    }

    @Override
    public void realmSet$id(Long value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$title() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.titleIndex);
    }

    @Override
    public void realmSet$title(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.titleIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.titleIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.titleIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.titleIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$year() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.yearIndex);
    }

    @Override
    public void realmSet$year(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.yearIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.yearIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.yearIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.yearIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$imdbID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.imdbIDIndex);
    }

    @Override
    public void realmSet$imdbID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.imdbIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.imdbIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.imdbIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.imdbIDIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$type() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.typeIndex);
    }

    @Override
    public void realmSet$type(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.typeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.typeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.typeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.typeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$poster() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.posterIndex);
    }

    @Override
    public void realmSet$poster(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.posterIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.posterIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.posterIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.posterIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Movie");
        builder.addProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("title", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("year", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("imdbID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("type", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addProperty("poster", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
         return expectedObjectSchemaInfo;
    }

    public static MovieColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (!sharedRealm.hasTable("class_Movie")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'Movie' class is missing from the schema for this Realm.");
        }
        Table table = sharedRealm.getTable("class_Movie");
        final long columnCount = table.getColumnCount();
        if (columnCount != 6) {
            if (columnCount < 6) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 6 but was " + columnCount);
            }
            if (allowExtraColumns) {
                RealmLog.debug("Field count is more than expected - expected 6 but was %1$d", columnCount);
            } else {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 6 but was " + columnCount);
            }
        }
        Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
        for (long i = 0; i < columnCount; i++) {
            columnTypes.put(table.getColumnName(i), table.getColumnType(i));
        }

        final MovieColumnInfo columnInfo = new MovieColumnInfo(sharedRealm, table);

        if (!table.hasPrimaryKey()) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
        } else {
            if (table.getPrimaryKey() != columnInfo.idIndex) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field id");
            }
        }

        if (!columnTypes.containsKey("id")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("id") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'Long' for field 'id' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.idIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
        }
        if (!table.hasSearchIndex(table.getColumnIndex("id"))) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
        }
        if (!columnTypes.containsKey("title")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'title' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("title") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'title' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.titleIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'title' is required. Either set @Required to field 'title' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("year")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'year' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("year") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'year' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.yearIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'year' is required. Either set @Required to field 'year' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("imdbID")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'imdbID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("imdbID") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'imdbID' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.imdbIDIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'imdbID' is required. Either set @Required to field 'imdbID' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("type")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'type' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("type") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'type' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.typeIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'type' is required. Either set @Required to field 'type' or migrate using RealmObjectSchema.setNullable().");
        }
        if (!columnTypes.containsKey("poster")) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'poster' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
        }
        if (columnTypes.get("poster") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'poster' in existing Realm file.");
        }
        if (!table.isColumnNullable(columnInfo.posterIndex)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'poster' is required. Either set @Required to field 'poster' or migrate using RealmObjectSchema.setNullable().");
        }

        return columnInfo;
    }

    public static String getTableName() {
        return "class_Movie";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static th.co.artxzilla.movierating.data.entity.Movie createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        th.co.artxzilla.movierating.data.entity.Movie obj = null;
        if (update) {
            Table table = realm.getTable(th.co.artxzilla.movierating.data.entity.Movie.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("id")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(th.co.artxzilla.movierating.data.entity.Movie.class), false, Collections.<String> emptyList());
                    obj = new io.realm.MovieRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.MovieRealmProxy) realm.createObjectInternal(th.co.artxzilla.movierating.data.entity.Movie.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.MovieRealmProxy) realm.createObjectInternal(th.co.artxzilla.movierating.data.entity.Movie.class, json.getLong("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }
        if (json.has("title")) {
            if (json.isNull("title")) {
                ((MovieRealmProxyInterface) obj).realmSet$title(null);
            } else {
                ((MovieRealmProxyInterface) obj).realmSet$title((String) json.getString("title"));
            }
        }
        if (json.has("year")) {
            if (json.isNull("year")) {
                ((MovieRealmProxyInterface) obj).realmSet$year(null);
            } else {
                ((MovieRealmProxyInterface) obj).realmSet$year((String) json.getString("year"));
            }
        }
        if (json.has("imdbID")) {
            if (json.isNull("imdbID")) {
                ((MovieRealmProxyInterface) obj).realmSet$imdbID(null);
            } else {
                ((MovieRealmProxyInterface) obj).realmSet$imdbID((String) json.getString("imdbID"));
            }
        }
        if (json.has("type")) {
            if (json.isNull("type")) {
                ((MovieRealmProxyInterface) obj).realmSet$type(null);
            } else {
                ((MovieRealmProxyInterface) obj).realmSet$type((String) json.getString("type"));
            }
        }
        if (json.has("poster")) {
            if (json.isNull("poster")) {
                ((MovieRealmProxyInterface) obj).realmSet$poster(null);
            } else {
                ((MovieRealmProxyInterface) obj).realmSet$poster((String) json.getString("poster"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static th.co.artxzilla.movierating.data.entity.Movie createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        th.co.artxzilla.movierating.data.entity.Movie obj = new th.co.artxzilla.movierating.data.entity.Movie();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MovieRealmProxyInterface) obj).realmSet$id(null);
                } else {
                    ((MovieRealmProxyInterface) obj).realmSet$id((long) reader.nextLong());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("title")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MovieRealmProxyInterface) obj).realmSet$title(null);
                } else {
                    ((MovieRealmProxyInterface) obj).realmSet$title((String) reader.nextString());
                }
            } else if (name.equals("year")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MovieRealmProxyInterface) obj).realmSet$year(null);
                } else {
                    ((MovieRealmProxyInterface) obj).realmSet$year((String) reader.nextString());
                }
            } else if (name.equals("imdbID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MovieRealmProxyInterface) obj).realmSet$imdbID(null);
                } else {
                    ((MovieRealmProxyInterface) obj).realmSet$imdbID((String) reader.nextString());
                }
            } else if (name.equals("type")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MovieRealmProxyInterface) obj).realmSet$type(null);
                } else {
                    ((MovieRealmProxyInterface) obj).realmSet$type((String) reader.nextString());
                }
            } else if (name.equals("poster")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MovieRealmProxyInterface) obj).realmSet$poster(null);
                } else {
                    ((MovieRealmProxyInterface) obj).realmSet$poster((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static th.co.artxzilla.movierating.data.entity.Movie copyOrUpdate(Realm realm, th.co.artxzilla.movierating.data.entity.Movie object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (th.co.artxzilla.movierating.data.entity.Movie) cachedRealmObject;
        }

        th.co.artxzilla.movierating.data.entity.Movie realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(th.co.artxzilla.movierating.data.entity.Movie.class);
            long pkColumnIndex = table.getPrimaryKey();
            Number value = ((MovieRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, value.longValue());
            }
            if (rowIndex != Table.NO_MATCH) {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(th.co.artxzilla.movierating.data.entity.Movie.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.MovieRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static th.co.artxzilla.movierating.data.entity.Movie copy(Realm realm, th.co.artxzilla.movierating.data.entity.Movie newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (th.co.artxzilla.movierating.data.entity.Movie) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        th.co.artxzilla.movierating.data.entity.Movie realmObject = realm.createObjectInternal(th.co.artxzilla.movierating.data.entity.Movie.class, ((MovieRealmProxyInterface) newObject).realmGet$id(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        MovieRealmProxyInterface realmObjectSource = (MovieRealmProxyInterface) newObject;
        MovieRealmProxyInterface realmObjectCopy = (MovieRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$title(realmObjectSource.realmGet$title());
        realmObjectCopy.realmSet$year(realmObjectSource.realmGet$year());
        realmObjectCopy.realmSet$imdbID(realmObjectSource.realmGet$imdbID());
        realmObjectCopy.realmSet$type(realmObjectSource.realmGet$type());
        realmObjectCopy.realmSet$poster(realmObjectSource.realmGet$poster());
        return realmObject;
    }

    public static long insert(Realm realm, th.co.artxzilla.movierating.data.entity.Movie object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(th.co.artxzilla.movierating.data.entity.Movie.class);
        long tableNativePtr = table.getNativePtr();
        MovieColumnInfo columnInfo = (MovieColumnInfo) realm.schema.getColumnInfo(th.co.artxzilla.movierating.data.entity.Movie.class);
        long pkColumnIndex = table.getPrimaryKey();
        Object primaryKeyValue = ((MovieRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((MovieRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, ((MovieRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$title = ((MovieRealmProxyInterface) object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
        }
        String realmGet$year = ((MovieRealmProxyInterface) object).realmGet$year();
        if (realmGet$year != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.yearIndex, rowIndex, realmGet$year, false);
        }
        String realmGet$imdbID = ((MovieRealmProxyInterface) object).realmGet$imdbID();
        if (realmGet$imdbID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.imdbIDIndex, rowIndex, realmGet$imdbID, false);
        }
        String realmGet$type = ((MovieRealmProxyInterface) object).realmGet$type();
        if (realmGet$type != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.typeIndex, rowIndex, realmGet$type, false);
        }
        String realmGet$poster = ((MovieRealmProxyInterface) object).realmGet$poster();
        if (realmGet$poster != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.posterIndex, rowIndex, realmGet$poster, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(th.co.artxzilla.movierating.data.entity.Movie.class);
        long tableNativePtr = table.getNativePtr();
        MovieColumnInfo columnInfo = (MovieColumnInfo) realm.schema.getColumnInfo(th.co.artxzilla.movierating.data.entity.Movie.class);
        long pkColumnIndex = table.getPrimaryKey();
        th.co.artxzilla.movierating.data.entity.Movie object = null;
        while (objects.hasNext()) {
            object = (th.co.artxzilla.movierating.data.entity.Movie) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((MovieRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((MovieRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, ((MovieRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$title = ((MovieRealmProxyInterface) object).realmGet$title();
            if (realmGet$title != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
            }
            String realmGet$year = ((MovieRealmProxyInterface) object).realmGet$year();
            if (realmGet$year != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.yearIndex, rowIndex, realmGet$year, false);
            }
            String realmGet$imdbID = ((MovieRealmProxyInterface) object).realmGet$imdbID();
            if (realmGet$imdbID != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.imdbIDIndex, rowIndex, realmGet$imdbID, false);
            }
            String realmGet$type = ((MovieRealmProxyInterface) object).realmGet$type();
            if (realmGet$type != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.typeIndex, rowIndex, realmGet$type, false);
            }
            String realmGet$poster = ((MovieRealmProxyInterface) object).realmGet$poster();
            if (realmGet$poster != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.posterIndex, rowIndex, realmGet$poster, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, th.co.artxzilla.movierating.data.entity.Movie object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(th.co.artxzilla.movierating.data.entity.Movie.class);
        long tableNativePtr = table.getNativePtr();
        MovieColumnInfo columnInfo = (MovieColumnInfo) realm.schema.getColumnInfo(th.co.artxzilla.movierating.data.entity.Movie.class);
        long pkColumnIndex = table.getPrimaryKey();
        Object primaryKeyValue = ((MovieRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((MovieRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, ((MovieRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, rowIndex);
        String realmGet$title = ((MovieRealmProxyInterface) object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex, false);
        }
        String realmGet$year = ((MovieRealmProxyInterface) object).realmGet$year();
        if (realmGet$year != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.yearIndex, rowIndex, realmGet$year, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.yearIndex, rowIndex, false);
        }
        String realmGet$imdbID = ((MovieRealmProxyInterface) object).realmGet$imdbID();
        if (realmGet$imdbID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.imdbIDIndex, rowIndex, realmGet$imdbID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.imdbIDIndex, rowIndex, false);
        }
        String realmGet$type = ((MovieRealmProxyInterface) object).realmGet$type();
        if (realmGet$type != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.typeIndex, rowIndex, realmGet$type, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.typeIndex, rowIndex, false);
        }
        String realmGet$poster = ((MovieRealmProxyInterface) object).realmGet$poster();
        if (realmGet$poster != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.posterIndex, rowIndex, realmGet$poster, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.posterIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(th.co.artxzilla.movierating.data.entity.Movie.class);
        long tableNativePtr = table.getNativePtr();
        MovieColumnInfo columnInfo = (MovieColumnInfo) realm.schema.getColumnInfo(th.co.artxzilla.movierating.data.entity.Movie.class);
        long pkColumnIndex = table.getPrimaryKey();
        th.co.artxzilla.movierating.data.entity.Movie object = null;
        while (objects.hasNext()) {
            object = (th.co.artxzilla.movierating.data.entity.Movie) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((MovieRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((MovieRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, ((MovieRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, rowIndex);
            String realmGet$title = ((MovieRealmProxyInterface) object).realmGet$title();
            if (realmGet$title != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex, false);
            }
            String realmGet$year = ((MovieRealmProxyInterface) object).realmGet$year();
            if (realmGet$year != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.yearIndex, rowIndex, realmGet$year, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.yearIndex, rowIndex, false);
            }
            String realmGet$imdbID = ((MovieRealmProxyInterface) object).realmGet$imdbID();
            if (realmGet$imdbID != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.imdbIDIndex, rowIndex, realmGet$imdbID, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.imdbIDIndex, rowIndex, false);
            }
            String realmGet$type = ((MovieRealmProxyInterface) object).realmGet$type();
            if (realmGet$type != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.typeIndex, rowIndex, realmGet$type, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.typeIndex, rowIndex, false);
            }
            String realmGet$poster = ((MovieRealmProxyInterface) object).realmGet$poster();
            if (realmGet$poster != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.posterIndex, rowIndex, realmGet$poster, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.posterIndex, rowIndex, false);
            }
        }
    }

    public static th.co.artxzilla.movierating.data.entity.Movie createDetachedCopy(th.co.artxzilla.movierating.data.entity.Movie realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        th.co.artxzilla.movierating.data.entity.Movie unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new th.co.artxzilla.movierating.data.entity.Movie();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (th.co.artxzilla.movierating.data.entity.Movie) cachedObject.object;
            }
            unmanagedObject = (th.co.artxzilla.movierating.data.entity.Movie) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        MovieRealmProxyInterface unmanagedCopy = (MovieRealmProxyInterface) unmanagedObject;
        MovieRealmProxyInterface realmSource = (MovieRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$title(realmSource.realmGet$title());
        unmanagedCopy.realmSet$year(realmSource.realmGet$year());
        unmanagedCopy.realmSet$imdbID(realmSource.realmGet$imdbID());
        unmanagedCopy.realmSet$type(realmSource.realmGet$type());
        unmanagedCopy.realmSet$poster(realmSource.realmGet$poster());
        return unmanagedObject;
    }

    static th.co.artxzilla.movierating.data.entity.Movie update(Realm realm, th.co.artxzilla.movierating.data.entity.Movie realmObject, th.co.artxzilla.movierating.data.entity.Movie newObject, Map<RealmModel, RealmObjectProxy> cache) {
        MovieRealmProxyInterface realmObjectTarget = (MovieRealmProxyInterface) realmObject;
        MovieRealmProxyInterface realmObjectSource = (MovieRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$title(realmObjectSource.realmGet$title());
        realmObjectTarget.realmSet$year(realmObjectSource.realmGet$year());
        realmObjectTarget.realmSet$imdbID(realmObjectSource.realmGet$imdbID());
        realmObjectTarget.realmSet$type(realmObjectSource.realmGet$type());
        realmObjectTarget.realmSet$poster(realmObjectSource.realmGet$poster());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Movie = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id() != null ? realmGet$id() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{title:");
        stringBuilder.append(realmGet$title() != null ? realmGet$title() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{year:");
        stringBuilder.append(realmGet$year() != null ? realmGet$year() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{imdbID:");
        stringBuilder.append(realmGet$imdbID() != null ? realmGet$imdbID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{type:");
        stringBuilder.append(realmGet$type() != null ? realmGet$type() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{poster:");
        stringBuilder.append(realmGet$poster() != null ? realmGet$poster() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieRealmProxy aMovie = (MovieRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aMovie.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aMovie.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aMovie.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
