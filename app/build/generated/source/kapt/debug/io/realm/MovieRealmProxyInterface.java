package io.realm;


public interface MovieRealmProxyInterface {
    public Long realmGet$id();
    public void realmSet$id(Long value);
    public String realmGet$title();
    public void realmSet$title(String value);
    public String realmGet$year();
    public void realmSet$year(String value);
    public String realmGet$imdbID();
    public void realmSet$imdbID(String value);
    public String realmGet$type();
    public void realmSet$type(String value);
    public String realmGet$poster();
    public void realmSet$poster(String value);
}
