package th.co.artxzilla.movierating;

public class BR {
        public static final int _all = 0;
        public static final int data = 1;
        public static final int id = 2;
        public static final int imdbID = 3;
        public static final int poster = 4;
        public static final int title = 5;
        public static final int type = 6;
        public static final int year = 7;
}