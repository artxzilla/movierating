package th.co.artxzilla.movierating.data.local

import th.co.artxzilla.movierating.data.entity.Movie

interface Dao {

    fun getMovieById(id: Long): LiveRealmData<Movie>
}

