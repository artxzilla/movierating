package th.co.artxzilla.movierating.data.remote

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import th.co.artxzilla.movierating.data.entity.SearchMoviesResult

interface OmdbApi {
    @GET("/")
    fun searchMovies(@Query("s") movieKeyword: String, @Query("apikey") apikey: String = "91b452f1"): Call<SearchMoviesResult>
}
