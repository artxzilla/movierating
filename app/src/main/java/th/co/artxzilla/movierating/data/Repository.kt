package th.co.artxzilla.movierating.data

import th.co.artxzilla.movierating.data.entity.Movie
import th.co.artxzilla.movierating.data.entity.SearchMoviesResult
import th.co.artxzilla.movierating.data.local.LiveRealmData

interface Repository {

    fun searchMovies(request: String, callback: (response: SearchMoviesResult?) -> Unit)

    fun getMovieById(id: Long): LiveRealmData<Movie>
}
