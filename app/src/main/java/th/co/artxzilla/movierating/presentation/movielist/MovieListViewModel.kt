package th.co.artxzilla.movierating.presentation.movielist

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Intent
import th.co.artxzilla.movierating.data.Repository
import th.co.artxzilla.movierating.presentation.searchmovie.SearchMovieActivity
import javax.inject.Inject

class MovieListViewModel : ViewModel() {

    private val TAG = "MovieListViewModel"

    @Inject
    lateinit var repository: Repository

    fun onClickActionGotoSearch(activity: Activity) {
        activity.startActivity(Intent(activity, SearchMovieActivity::class.java))
    }
}