package th.co.artxzilla.movierating.util

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AlertDialog
import android.view.inputmethod.InputMethodManager
import th.co.artxzilla.movierating.Application.Companion.mApplicationContext

object Utils {
    fun messageDialogUtil(title: String?, msg: String?) {
        val builder = AlertDialog.Builder(mApplicationContext)
        builder.setTitle(title)
        builder.setMessage(msg)
        builder.setPositiveButton("ตกลง") { dialog, _ -> dialog.dismiss() }
        builder.setCancelable(false)
        builder.create()
        builder.show()
    }

    fun getWaitingDialog(): ProgressDialog {
        val progressDialog = ProgressDialog(mApplicationContext)
        progressDialog.setMessage("กรุณารอสักครู่")
        progressDialog.isIndeterminate = false
        progressDialog.setCancelable(false)
        return progressDialog
    }

    fun hideSoftKeyboard(activity: Activity) {
        // hide soft keyboard
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = mApplicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
    }
}
