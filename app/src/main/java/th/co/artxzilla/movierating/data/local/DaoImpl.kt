package th.co.artxzilla.movierating.data.local

import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults
import th.co.artxzilla.movierating.data.entity.Movie

class DaoImpl : Dao {

    private fun <T : RealmModel> RealmResults<T>.asLiveData() = LiveRealmData(this)

    private var realm = Realm.getDefaultInstance()

    override fun getMovieById(id: Long): LiveRealmData<Movie> {
        return realm.where(Movie::class.java).equalTo(Movie::id.name, id).findAllAsync().asLiveData()
    }

}

