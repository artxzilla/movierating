package th.co.artxzilla.movierating

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import th.co.artxzilla.movierating.data.di.LocalDataModule
import th.co.artxzilla.movierating.data.di.RemoteDataModule
import th.co.artxzilla.movierating.data.di.RepositoryModule
import th.co.artxzilla.movierating.presentation.di.AppComponent
import th.co.artxzilla.movierating.presentation.di.DaggerAppComponent

class Application : Application(), Application.ActivityLifecycleCallbacks {

    companion object {
        lateinit var mApplicationContext: Context
        lateinit var component: AppComponent
    }

    private val BASE_URL = "https://www.omdbapi.com"

    override fun onCreate() {
        super.onCreate()
        buildDependencyGraph()

        registerActivityLifecycleCallbacks(this)
    }

    private fun buildDependencyGraph() {
        mApplicationContext = applicationContext
        component = DaggerAppComponent.builder()
                .localDataModule(LocalDataModule(applicationContext))
                .remoteDataModule(RemoteDataModule(BASE_URL))
                .repositoryModule(RepositoryModule())
                .build()
    }

    override fun onActivityCreated(activity: Activity, p1: Bundle?) {
        activity.let { mApplicationContext = it }
    }

    override fun onActivityStarted(activity: Activity) {

    }

    override fun onActivityResumed(activity: Activity) {
        activity.let { mApplicationContext = it }
    }

    override fun onActivitySaveInstanceState(activity: Activity, p1: Bundle?) {

    }

    override fun onActivityPaused(activity: Activity) {

    }

    override fun onActivityStopped(activity: Activity) {

    }

    override fun onActivityDestroyed(activity: Activity) {

    }
}

