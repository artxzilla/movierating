package th.co.artxzilla.movierating.presentation.searchmovie

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Intent
import th.co.artxzilla.movierating.data.Repository
import th.co.artxzilla.movierating.data.entity.Movie
import th.co.artxzilla.movierating.presentation.moviedetail.MovieDetailActivity
import javax.inject.Inject

class SearchMovieViewModel : ViewModel() {

    private val TAG = "SearchMovieViewModel"

    @Inject
    lateinit var repository: Repository

    fun searchMovies(movieKeyword: String, callback: (result: List<Movie>?) -> Unit) {
        repository.searchMovies(movieKeyword, {
            callback.invoke(it?.search)
        })
    }

    fun onClickMovieItem(movie: Movie, activity: Activity) {
        activity.startActivity(Intent(activity, MovieDetailActivity::class.java))
    }
}