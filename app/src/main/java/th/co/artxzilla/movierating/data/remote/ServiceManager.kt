package th.co.artxzilla.movierating.data.remote

import android.app.ProgressDialog
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import th.co.artxzilla.movierating.util.Utils
import th.co.artxzilla.movierating.util.Utils.messageDialogUtil

object ServiceManager {

    val TOKEN_EXPIRED_TAG = "TOKEN_EXPIRED"
    private val TAG = "ServiceManager"

    private var waitingDialog: ProgressDialog? = null

    fun <T> service(closeLoading: Boolean, call: Call<T>, callback: (result: T?) -> Unit) {
        // check internet
        if (!Utils.isNetworkAvailable()) {
            messageDialogUtil(null, "network_error_message")
        } else {
            showWaitingDialog()
            call.enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    if (response.code() == 200) {
                        callback(response.body())
                        if (closeLoading) {
                            dismissWaitingDialog()
                        }
                    } else {
                        Log.e(TAG, "[RESPONSE CODE " + response.code() + "] " + response.message())
                        dismissWaitingDialog()
                        messageDialogUtil(null, response.message())
                    }
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    Log.e(TAG, "onFailure", t)
                    dismissWaitingDialog()
                    if ("connect timed out" == t.message) {
                        messageDialogUtil(null, "network_error_message")
                    } else {
                        messageDialogUtil(null, "default_error_message")
                    }
                }
            })
        }
    }

    private fun showWaitingDialog() {
        if (waitingDialog == null) {
            waitingDialog = Utils.getWaitingDialog()
        }
        waitingDialog!!.show()
    }

    private fun dismissWaitingDialog() {
        if (waitingDialog != null) {
            waitingDialog!!.dismiss()
            waitingDialog = null
        }
    }
}
