package th.co.artxzilla.movierating.presentation.movielist

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import th.co.artxzilla.movierating.Application
import th.co.artxzilla.movierating.R

class MovieListActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(MovieListViewModel::class.java).also {
            Application.component.inject(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_movielist, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_gotoseach -> {
            // todo goto serchmovie
            viewModel.onClickActionGotoSearch(this)
            true
        }
        else -> false
    }
}
