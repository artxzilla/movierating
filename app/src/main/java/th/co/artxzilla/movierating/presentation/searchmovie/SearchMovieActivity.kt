package th.co.artxzilla.movierating.presentation.searchmovie

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import th.co.artxzilla.movierating.Application
import th.co.artxzilla.movierating.R
import th.co.artxzilla.movierating.databinding.ActivitySearchMovieBinding

class SearchMovieActivity : AppCompatActivity() {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivitySearchMovieBinding>(this, R.layout.activity_search_movie)
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(SearchMovieViewModel::class.java).also {
            Application.component.inject(it)
        }
    }

    private val searchMovieAdapter = SearchMovieAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initInstance()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_search -> {
            true
        }
        android.R.id.home -> {
            finish()
            true
        }
        else -> false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_searchmovie, menu)

        val searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView.isFocusable = true
        searchView.requestFocus()
        searchView.requestFocusFromTouch()
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val searchMenuItem = menu.findItem(R.id.action_search).actionView as SearchView

        searchMenuItem.isFocusable = false
        searchMenuItem.isIconified = false
        searchMenuItem.clearFocus()

        searchMenuItem.maxWidth = Integer.MAX_VALUE
        searchMenuItem.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                // todo
//                searchMovie(query)
                return true
            }

            override fun onQueryTextChange(query: String): Boolean {
                searchMovie(query)
                return true
            }
        })

        val searchEditText = searchMenuItem.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        searchEditText.isFocusable = true
        searchEditText.setTextColor(ContextCompat.getColor(this, android.R.color.white))
        searchEditText.setHintTextColor(ContextCompat.getColor(this, R.color.material_grey_100))

        searchEditText.requestFocus()

        return super.onPrepareOptionsMenu(menu)
    }

    private fun initInstance() {
        binding.rcvSearchmovieList.apply {
            setHasFixedSize(true)

            val linearLayoutManager = LinearLayoutManager(context)
            linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
            layoutManager = linearLayoutManager

            searchMovieAdapter.onMovieItemClickListener = {
                viewModel.onClickMovieItem(it, this@SearchMovieActivity)
            }

            adapter = searchMovieAdapter
        }
    }

    private fun searchMovie(keyword: String?) {
        if (keyword != null && keyword.isNotEmpty()) {
            viewModel.searchMovies(keyword, {
                if (it != null) searchMovieAdapter.dataList = it
            })
        } else {
            searchMovieAdapter.dataList = emptyList()
        }
    }
}
