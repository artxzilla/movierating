package th.co.artxzilla.movierating.presentation.moviedetail

import android.arch.lifecycle.ViewModel
import th.co.artxzilla.movierating.data.Repository
import th.co.artxzilla.movierating.data.entity.Movie
import th.co.artxzilla.movierating.data.local.LiveRealmData
import javax.inject.Inject

class MovieDetailViewModel : ViewModel() {

    private val TAG = "MovieDetailViewModel"

    @Inject
    lateinit var repository: Repository

    fun getMovieById(id: Long): LiveRealmData<Movie> =
            repository.getMovieById(id)
}