package th.co.artxzilla.movierating.data.entity

import com.google.gson.annotations.SerializedName

open class SearchMoviesResult {
    @SerializedName("Search")
    var search: List<Movie>? = null
}