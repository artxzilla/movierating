package th.co.artxzilla.movierating.presentation.di

import dagger.Component
import th.co.artxzilla.movierating.data.di.LocalDataModule
import th.co.artxzilla.movierating.data.di.RemoteDataModule
import th.co.artxzilla.movierating.data.di.RepositoryModule
import th.co.artxzilla.movierating.presentation.moviedetail.MovieDetailViewModel
import th.co.artxzilla.movierating.presentation.movielist.MovieListViewModel
import th.co.artxzilla.movierating.presentation.searchmovie.SearchMovieViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            (LocalDataModule::class),
            (RemoteDataModule::class),

            (RepositoryModule::class)
        ]
)
interface AppComponent {

    fun inject(movieListViewModel: MovieListViewModel)

    fun inject(searchMovieViewModel: SearchMovieViewModel)

    fun inject(movieDetailViewModel: MovieDetailViewModel)

}

