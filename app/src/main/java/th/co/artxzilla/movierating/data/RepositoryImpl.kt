package th.co.artxzilla.movierating.data

import io.reactivex.Scheduler
import th.co.artxzilla.movierating.data.entity.Movie
import th.co.artxzilla.movierating.data.entity.SearchMoviesResult
import th.co.artxzilla.movierating.data.local.Dao
import th.co.artxzilla.movierating.data.local.LiveRealmData
import th.co.artxzilla.movierating.data.remote.OmdbApi
import th.co.artxzilla.movierating.data.remote.ServiceManager

class RepositoryImpl(private val localSource: Dao, private val remoteSource: OmdbApi, private val scheduler: Scheduler) : Repository {
    override fun searchMovies(request: String, callback: (result: SearchMoviesResult?) -> Unit) {
        ServiceManager.service(true, remoteSource.searchMovies(request), callback)
    }

    override fun getMovieById(id: Long): LiveRealmData<Movie> =
            localSource.getMovieById(id)
}