package th.co.artxzilla.movierating.presentation.searchmovie

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import th.co.artxzilla.movierating.Application.Companion.mApplicationContext
import th.co.artxzilla.movierating.R
import th.co.artxzilla.movierating.data.entity.Movie
import th.co.artxzilla.movierating.databinding.ItemSearchMovieBinding
import kotlin.properties.Delegates

class SearchMovieAdapter : RecyclerView.Adapter<SearchMovieAdapter.ViewHolder>() {

    var dataList by Delegates.observable(listOf<Movie>()) { _, _, _ -> notifyDataSetChanged() }
    var onMovieItemClickListener: ((movie: Movie) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_movie, parent, false)
        return ViewHolder(view, onMovieItemClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding!!.data = dataList[position]

        // todo move to databinding
        Glide.with(mApplicationContext).load(dataList[position].poster).into(holder.binding!!.imgItemserachmovieImage)
    }

    override fun getItemCount() = dataList.size

    class ViewHolder(view: View, onMovieItemClickListener: ((movie: Movie) -> Unit)?) : RecyclerView.ViewHolder(view) {
        var binding: ItemSearchMovieBinding? = null

        init {
            binding = DataBindingUtil.bind(view)
            view.setOnClickListener { onMovieItemClickListener?.invoke(binding!!.data!!) }
        }
    }
}