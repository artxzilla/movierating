package th.co.artxzilla.movierating.data.entity

import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.databinding.Observable
import android.databinding.PropertyChangeRegistry
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.gson.annotations.SerializedName
import io.realm.RealmModel
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import th.co.artxzilla.movierating.Application.Companion.mApplicationContext
import th.co.artxzilla.movierating.BR


@RealmClass
open class Movie : RealmModel, Observable {
    @SerializedName("id")
    @PrimaryKey
    var id: Long? = null
        @Bindable get
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.id)
            }
        }

    @SerializedName("Title")
    var title: String? = null
        @Bindable get
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.title)
            }
        }

    @SerializedName("Year")
    var year: String? = null
        @Bindable get
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.year)
            }
        }

    @SerializedName("imdbID")
    var imdbID: String? = null
        @Bindable get
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.imdbID)
            }
        }

    @SerializedName("Type")
    var type: String? = null
        @Bindable get
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.type)
            }
        }

    @SerializedName("Poster")
    var poster: String? = null
        @Bindable get
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.poster)
            }
        }

    // from base observable
    @Ignore
    @Transient
    private var mCallbacks: PropertyChangeRegistry? = null

    // from observable interface
    @Synchronized
    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        if (mCallbacks == null) {
            mCallbacks = PropertyChangeRegistry()
        }
        mCallbacks!!.add(callback)
    }

    // from observable interface
    @Synchronized
    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        if (mCallbacks != null) {
            mCallbacks!!.remove(callback)
        }
    }

    private fun notifyPropertyChanged(fieldId: Int) {
        if (mCallbacks != null) {
            mCallbacks!!.notifyCallbacks(this, fieldId, null)
        }
    }
}