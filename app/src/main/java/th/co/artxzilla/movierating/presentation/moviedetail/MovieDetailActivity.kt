package th.co.artxzilla.movierating.presentation.moviedetail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import th.co.artxzilla.movierating.Application
import th.co.artxzilla.movierating.R
import th.co.artxzilla.movierating.databinding.ActivityMovieDetailBinding

class MovieDetailActivity : AppCompatActivity() {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMovieDetailBinding>(this, R.layout.activity_movie_detail)
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(MovieDetailViewModel::class.java).also {
            Application.component.inject(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initInstance()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> false
    }

    private fun initInstance() {
        // todo get id from intent
        val id = 1L

        viewModel.getMovieById(id).observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                binding.apply {
                    data = it.first()
                    executePendingBindings()
                }
            } else {
                // todo handler not found
                Toast.makeText(this, "Not Found.", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
