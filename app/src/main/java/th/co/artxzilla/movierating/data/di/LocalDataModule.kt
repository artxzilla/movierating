package th.co.artxzilla.movierating.data.di

import android.content.Context
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import th.co.artxzilla.movierating.data.local.Dao
import th.co.artxzilla.movierating.data.local.DaoImpl
import javax.inject.Singleton

@Module
class LocalDataModule(private val context: Context) {

    private val DB_FILE_NAME = "movierating.realm"

    @Provides
    @Singleton
    fun provideDao(): Dao {
        Realm.init(context)
        val realmConfig = RealmConfiguration.Builder()
                .name(DB_FILE_NAME)
                .schemaVersion(3)
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(realmConfig)
        return DaoImpl()
    }

}
