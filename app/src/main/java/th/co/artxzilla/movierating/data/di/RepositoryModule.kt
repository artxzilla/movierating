package th.co.artxzilla.movierating.data.di

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import th.co.artxzilla.movierating.data.Repository
import th.co.artxzilla.movierating.data.RepositoryImpl
import th.co.artxzilla.movierating.data.local.Dao
import th.co.artxzilla.movierating.data.remote.OmdbApi
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(localSource: Dao, remoteSource: OmdbApi): Repository
            = RepositoryImpl(localSource, remoteSource, Schedulers.io())
}

